package tk.giesecke.cctvview.Onvif;

import android.annotation.SuppressLint;

import org.apache.commons.codec.digest.MessageDigestAlgorithms;
import org.apache.mina.util.Base64;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Random;
import java.util.SimpleTimeZone;
import java.util.TimeZone;

import static tk.giesecke.cctvview.CCTVview.selectedDevice;

public class OnvifHeaderBody {

	private static String utcTime;
	private static String nonce;

	public static String getAuthorizationHeader() {
		utcTime = getUTCTime();
		String envelopePart;
		String authorizationPart = "";
		envelopePart = "<SOAP-ENV:Envelope " +
				"xmlns:SOAP-ENV=\"http://www.w3.org/2003/05/soap-envelope\" " +
				"xmlns:SOAP-ENC=\"http://www.w3.org/2003/05/soap-encoding\" " +
				"xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" " +
				"xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" " +
				"xmlns:wsa=\"http://schemas.xmlsoap.org/ws/2004/08/addressing\" " +
				"xmlns:wsdd=\"http://schemas.xmlsoap.org/ws/2005/04/discovery\" " +
				"xmlns:chan=\"http://schemas.microsoft.com/ws/2005/02/duplex\" " +
				"xmlns:wsa5=\"http://www.w3.org/2005/08/addressing\" " +
				"xmlns:xmime=\"http://tempuri.org/xmime.xsd\" " +
				"xmlns:xop=\"http://www.w3.org/2004/08/xop/include\" " +
				"xmlns:tt=\"http://www.onvif.org/ver10/schema\" " +
				"xmlns:wsrfbf=\"http://docs.oasis-open.org/wsrf/bf-2\" " +
				"xmlns:wstop=\"http://docs.oasis-open.org/wsn/t-1\" " +
				"xmlns:wsrfr=\"http://docs.oasis-open.org/wsrf/r-2\" " +
				"xmlns:tdn=\"http://www.onvif.org/ver10/network/wsdl\" " +
				"xmlns:tds=\"http://www.onvif.org/ver10/device/wsdl\" " +
				"xmlns:tev=\"http://www.onvif.org/ver10/events/wsdl\" " +
				"xmlns:wsnt=\"http://docs.oasis-open.org/wsn/b-2\" " +
				"xmlns:tptz=\"http://www.onvif.org/ver20/ptz/wsdl\" " +
                "xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\" " +
				"xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\" " +
				"xmlns:trt=\"http://www.onvif.org/ver10/media/wsdl\">";
		if (!selectedDevice.userName.equalsIgnoreCase("")) {
			try {
				//From the spec: Password_Digest = Base64 ( SHA-1 ( nonce + created + password ) )
				//Make the nonce
				SecureRandom rand = new SecureRandom();
				byte[] nonceBytes = new byte[16];
				rand.nextBytes(nonceBytes);
				nonce = new String(Base64.encodeBase64(nonceBytes));

				@SuppressLint("SimpleDateFormat") DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
				df.setTimeZone(TimeZone.getTimeZone("UTC"));
				String createdDate = df.format(Calendar.getInstance().getTime());
				byte[] createdDateBytes = createdDate.getBytes("UTF-8");

				//Make the password
				byte[] passwordBytes = selectedDevice.passWord.getBytes("UTF-8");

				//SHA-1 hash the bunch of it.
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				baos.write(nonceBytes);
				baos.write(createdDateBytes);
				baos.write(passwordBytes);
				MessageDigest md = MessageDigest.getInstance("SHA-1");
				byte[] digestedPassword = md.digest(baos.toByteArray());

				//Encode the password and nonce for sending
				String passwordB64 = new String(Base64.encodeBase64(digestedPassword));
				String nonceB64 = new String(Base64.encodeBase64(nonceBytes));

				authorizationPart = "<SOAP-ENV:Header>" +
						"<wsse:Security>" +
						"<wsse:UsernameToken>" +
						"<wsse:Username>" +
						selectedDevice.userName +
						"</wsse:Username>" +
						"<wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest\">" +
						passwordB64 +
						"</wsse:Password>" +
						"<wsse:Nonce>" +
						nonceB64 +
						"</wsse:Nonce>" +
						"<wsu:Created>" +
						createdDate +
						"</wsu:Created>" +
						"</wsse:UsernameToken>" +
						"</wsse:Security>" +
						"</SOAP-ENV:Header>";

			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return envelopePart + authorizationPart + "<SOAP-ENV:Body>";
	}

	@SuppressWarnings("SameReturnValue")
	public static String getEnvelopeEnd() {
		return "</SOAP-ENV:Body></SOAP-ENV:Envelope>";
	}

	private static String getUTCTime() {
		@SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-d'T'HH:mm:ss'Z'");
		sdf.setTimeZone(new SimpleTimeZone(SimpleTimeZone.UTC_TIME, "UTC"));
		Calendar cal = new GregorianCalendar(TimeZone.getTimeZone("UTC"));
		return sdf.format(cal.getTime());
	}

	public static String simpleSoapFormatter(String unformattedStr) {
		return unformattedStr.replace("><", ">\n<");
	}
}
